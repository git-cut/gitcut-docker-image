FROM registry.gitlab.com/git-cut/gitcut-docker-base-image:latest

ENV GITCUT_PROJECT_ID=21854466
ARG CI_JOB_TOKEN

RUN pip3 install --extra-index-url https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/api/v4/projects/${GITCUT_PROJECT_ID}/packages/pypi/simple gitcut

RUN npm config set @git-cut:registry https://gitlab.com/api/v4/projects/${GITCUT_PROJECT_ID}/packages/npm/
RUN npm config set '//gitlab.com/api/v4/projects/${GITCUT_PROJECT_ID}/packages/npm/:_authToken' ${CI_JOB_TOKEN}
RUN npm install -g @git-cut/gitcut
